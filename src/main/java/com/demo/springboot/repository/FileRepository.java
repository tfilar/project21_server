package com.demo.springboot.repository;

import com.demo.springboot.domain.entity.Recipient;

import java.util.List;

public interface FileRepository {

    boolean findFile(List<Recipient> temp);
}
