package com.demo.springboot.repository.impl;

import com.demo.springboot.domain.entity.Recipient;
import com.demo.springboot.repository.FileRepository;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FileRepositoryImpl implements FileRepository {

    @Override
    public boolean findFile(List<Recipient> recipients) {
        FileReader fileReader;
        String line;

        try {
            fileReader = new FileReader("email_backup.csv");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                if (!(line.contains("recipient;title;"))) {
                    recipients.add(new Recipient(line.substring(0, line.indexOf(";", 2))));
                }
            }
            fileReader.close();
            findTheSame(recipients);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void findTheSame(List<Recipient> recipients) {
        List<Recipient> toRemove = new ArrayList<>();
        List<Integer> removedIndex = new ArrayList<>();
        for(int firstIterator = 0; firstIterator < recipients.size(); firstIterator++){
            for(int secondIterator = 1; secondIterator < recipients.size(); secondIterator++){
                if(secondIterator != firstIterator &&
                        !(removedIndex.contains(firstIterator) || removedIndex.contains(secondIterator)) &&
                            (recipients.get(firstIterator).getRecipient()).
                                    equals(recipients.get(secondIterator).getRecipient())) {
                    toRemove.add(recipients.get(secondIterator));
                    removedIndex.add(secondIterator);
                }
            }
        }
        recipients.removeAll(toRemove);
    }
}
