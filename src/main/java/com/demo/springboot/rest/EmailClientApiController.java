package com.demo.springboot.rest;


import com.demo.springboot.domain.dto.ContactsDto;
import com.demo.springboot.domain.dto.UserDataDto;
import com.demo.springboot.domain.entity.Recipient;
import com.demo.springboot.repository.FileRepository;
import com.demo.springboot.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/email")
public class EmailClientApiController {

    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private MailService mailService;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailClientApiController.class);

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public ResponseEntity<Void> mail(@RequestBody UserDataDto userDataDto) throws MessagingException {
        LOGGER.info("Nazwa odbiorcy: {}, Tytuł: {}, Treść: {} ",
        userDataDto.getRecipient(),
        userDataDto.getTitle(),
        userDataDto.getContent());

        return false != mailService.sendMail(userDataDto) ?
                new ResponseEntity<>(HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public ResponseEntity<ContactsDto> mail() {
        List<Recipient> recipients = new ArrayList<>();
        final boolean status = fileRepository.findFile(recipients);

        final ContactsDto contactsDto = new ContactsDto(recipients);
        LOGGER.info("Nazwa odbiorcow: {} ",
                contactsDto.getContacts());

        return false != status ?
                new ResponseEntity<>(contactsDto, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
