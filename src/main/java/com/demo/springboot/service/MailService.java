package com.demo.springboot.service;

import com.demo.springboot.domain.dto.UserDataDto;

import javax.mail.MessagingException;

public interface MailService {

    boolean sendMail(UserDataDto userDataDto) throws MessagingException;
    void saveFile(UserDataDto userDataDto);

}
