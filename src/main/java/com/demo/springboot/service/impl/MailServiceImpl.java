package com.demo.springboot.service.impl;

import com.demo.springboot.domain.dto.UserDataDto;
import com.demo.springboot.service.MailService;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

@Service
public class MailServiceImpl implements MailService {

    private String recipent;
    private final String USER = "emailclientpwsz@gmail.com";
    private final String HOST = "smtp.gmail.com";
    private final int PORT = 465;
    private final String PASSWORD = "krystiantobiaszartur";
    private Properties properties;
    private Session session;
    private MimeMessage mimeMessage;
    private Transport transport;

    @Override
    public boolean sendMail(UserDataDto userDataDto) {
        try {
            if(userDataDto.getContent().equals("") || userDataDto.getTitle().equals("")) {
                return false;
            }
            recipent = userDataDto.getRecipient();
            properties = System.getProperties();
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.port", "587");

            session = Session.getDefaultInstance(properties, null);

            mimeMessage = new MimeMessage(session);
            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipent));
            mimeMessage.setSubject(userDataDto.getTitle());
            mimeMessage.setContent(userDataDto.getContent(), "text/html");
            transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", USER, PASSWORD);
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            transport.close();
            saveFile(userDataDto);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void saveFile(UserDataDto userDataDto) {
        try {
            BufferedWriter save = new BufferedWriter(new FileWriter("email_backup.csv", true));
            save.write(userDataDto.toString());
            save.newLine();
            save.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
