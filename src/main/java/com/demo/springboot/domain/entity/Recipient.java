package com.demo.springboot.domain.entity;

public class Recipient {

    private String recipient;

    public Recipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipient() {
        return recipient;
    }

    @Override
    public String toString() {
        return "Recipient{" +
                "recipient='" + recipient + '\'' +
                '}';
    }
}
