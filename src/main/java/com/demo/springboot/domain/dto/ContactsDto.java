package com.demo.springboot.domain.dto;

import com.demo.springboot.domain.entity.Recipient;

import java.io.*;
import java.util.List;

public class ContactsDto implements Serializable {

    private List<Recipient> contacts;

    public ContactsDto(List<Recipient> contacts) {
        this.contacts = contacts;
    }

    public List<Recipient> getContacts() {
        return contacts;
    }

    @Override
    public String toString() {
        return "ContactsDto{" +
                "contacts=" + contacts +
                '}';
    }
}
