package com.demo.springboot.domain.dto;

import java.io.Serializable;

public class UserDataDto implements Serializable {

    private String recipient;
    private String title;
    private String content;

    public UserDataDto() {
    }

    public UserDataDto(String recipent, String title, String content) {
        this.recipient = recipent;
        this.title = title;
        this.content = content;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return recipient + ';'
                + title + ';'
                + content + ';';
    }
}
